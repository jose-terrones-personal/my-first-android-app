package com.example.myfirstapp.main.viewholders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myfirstapp.R

class ProfileViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun addContentView(
        holder: ProfileViewHolder,
        position: Int,
        data: Array<String>
    ) {
        val textView = holder.itemView.findViewById<TextView>(R.id.profile_row_text_view)
        textView.text = data[position]
    }
}

