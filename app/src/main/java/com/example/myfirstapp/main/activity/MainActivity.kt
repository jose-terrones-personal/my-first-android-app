package com.example.myfirstapp.main.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myfirstapp.R
import com.example.myfirstapp.main.fragments.LandingPage

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_myfirstapp)

        addFragment(savedInstanceState == null)
    }

    private fun addFragment(isNull: Boolean) {
        if (isNull) {
            val addedFragment = LandingPage()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragmentContainer, addedFragment)

            // addToBackStack accepts a string
            // LandingPage()::class.java.simpleName could be passed to it
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }

}
