package com.example.myfirstapp.main.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myfirstapp.R
import com.example.myfirstapp.main.States
import com.example.myfirstapp.main.adapters.ProfileAdapter
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfilePage : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    companion object {
        const val TITLE_KEY = "ProfilePage::title"
        const val COLOR_KEY = "ProfilePage::color"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewManager = LinearLayoutManager(context)
        viewAdapter = ProfileAdapter(arrayOf("profile1", "profile2", "profile3"))

        recyclerView = profile_recycle_view.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        arguments?.let {
            it.getString(COLOR_KEY)?.let { state ->
                when (state) {
                    States.YELLOW.value -> setProfileArguments(
                        States.YELLOW.value,
                        it.getString(TITLE_KEY)
                    )
                    States.LIGHT_BLUE.value -> setProfileArguments(
                        States.LIGHT_BLUE.value,
                        it.getString(TITLE_KEY)
                    )
                    States.ORANGE.value -> setProfileArguments(
                        States.ORANGE.value,
                        it.getString(TITLE_KEY)
                    )
                    States.DARK_BLUE.value -> setProfileArguments(
                        States.DARK_BLUE.value,
                        it.getString(TITLE_KEY)
                    )
                    else -> setProfileArguments(States.YELLOW.value, it.getString(TITLE_KEY))
                }
            }
        }
//        if (!arguments?.isEmpty!!) {
//            profile_page_title.text = arguments?.getString(TITLE_KEY)
//        }
    }

    private fun setProfileArguments(color: String, title: String?) {
        profile_page_title.text = title
        profileLayout.setBackgroundColor(Color.parseColor(color))
    }
}