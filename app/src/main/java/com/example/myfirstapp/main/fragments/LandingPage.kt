package com.example.myfirstapp.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myfirstapp.R
import kotlinx.android.synthetic.main.activity_myfirstapp.view.*
import kotlinx.android.synthetic.main.fragment_landingpage.*

class LandingPage: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_landingpage, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ready_button.setOnClickListener {
            swapFragmentView()
        }
    }

    private fun swapFragmentView() {
        val addedFragment = HomePage()
        activity?.supportFragmentManager?.beginTransaction()?.apply {
            replace(R.id.fragmentContainer, addedFragment)
            addToBackStack(null)
            commit()
        }
    }
}