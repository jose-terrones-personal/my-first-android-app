package com.example.myfirstapp.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myfirstapp.R
import com.example.myfirstapp.main.States
import kotlinx.android.synthetic.main.fragment_homepage.*

class HomePage : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        val bundle = Bundle()
        when (v?.id) {
            profile_view_one.id -> {
                setBundleArguments(bundle, States.YELLOW.value)
            }
            profile_view_two.id -> {
                setBundleArguments(bundle, States.LIGHT_BLUE.value)
            }
            profile_view_three.id -> {
                setBundleArguments(bundle, States.ORANGE.value)
            }
            profile_view_four.id -> {
                setBundleArguments(bundle, States.DARK_BLUE.value)
            }
            else -> redirectToProfilePage(bundle)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_homepage, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        clickListeners()
    }

    private fun clickListeners() {
        profile_view_one.setOnClickListener(this)
        profile_view_two.setOnClickListener(this)
        profile_view_three.setOnClickListener(this)
        profile_view_four.setOnClickListener(this)
    }

    private fun redirectToProfilePage(bundle: Bundle) {
        val addedFragment = ProfilePage()

        if (!bundle.isEmpty) addedFragment.arguments = bundle

        activity?.supportFragmentManager?.beginTransaction()?.apply {
            replace(R.id.fragmentContainer, addedFragment)
            addToBackStack(null)
            commit()
        }
    }

    private fun setBundleArguments(bundle: Bundle, color: String) {
        bundle.putString(ProfilePage.TITLE_KEY, "Profile Title")
        bundle.putString(ProfilePage.COLOR_KEY, color)
        redirectToProfilePage(bundle)
    }
}