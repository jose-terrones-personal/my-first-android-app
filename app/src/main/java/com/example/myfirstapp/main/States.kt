package com.example.myfirstapp.main

enum class States(val value: String) {
    YELLOW("#F3C952"),
    ORANGE("#D9571F"),
    DARK_BLUE("#0D3166"),
    LIGHT_BLUE("#59C2CB")
}